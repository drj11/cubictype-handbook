# Glossary

This glossary is intended to cover terms as they are used at
CubicType and _not_ be a comprehensive gloss of all uses of all
terms.

As policy CubicType should use terms that have been standardised
where possible.


## General / Miscellaneous

- font / face / typeface: i'm supposed to care about the
  technical difference.
  Modern usage is that one typeface has
  many fonts (Regular/Bold/Italic for emphatic style).
  At CubicType we use font for either concept.
  Note that the menu to choose fonts in computer programs is
  universally called "Fonts".

- drawing: refers to the particular shape of a single instance
  of a single glyph (including the design extras like background
  and guidelines that you might only see in the editor, if
  applicable).


## Design / Style / Genre

- Color: conventionally means _darkness_. Prefer grey value.

- Contrast: conventionally refers to _thick/thin_ contrast
  (by analogy with dark/light i suppose).
  Prefer _thick/thin contrast_ for this.

- Value: average black value. More value means more black.
  Beware: not commonly adopted.

- Monoline: Each stroke has a constant width.


## Shapes

- oblate: wider than tall (for example, the `/O` in _Adonis_ or
  _Blair);

- prolate: taller than wider (for example, the `/O` in most
  common book fonts);


## Contours and so on

- FDU: Font Design Unit.
  The unit on the grid on which each glyph is drawn.
  The size of the em-box is specified in FDU and governs how the
  glyphs scale.
  For 21st Century OpenType designs the em-box is conventionally
  1000 units.
  CubicType designs currently use a variety of FDU.
  In some cases the actual design grid may be different and
  the drawings scaled when compiling binaries.

- LSB/RSB: Left side-bearing, Right side-bearing.
  As conventional in TrueType outline fonts,
  the distance in FDU between the side of the glyph and the
  left- or right-most node in the outlines.
  Or some equivalent way of specifying it (Glyphs.app allows you
  to specify the side bearings at a certain height, or
  computed from the side bearings of other glyphs).


## Calligraphy

(The Origin of the Serif may be useful)

- _cant_ [ORIGIN] p15 the angle the nib makes, 0° is horizontal,
  increasing anti-clockwise.


## Glyph names

If referring to a particular font use the actual glyph name.

Otherwise there are a few ways to refer to a glyph:

- full unicode: U+225B STAR EQUALS
- its Adobe Glyph List name
- its name in Glyphs.app

as a concession, or where the ambiguity is useful,
refer to a Unicode point using only some of its full name:
W WITH DOT ABOVE (full name starts with LATIN CAPITAL LETTER or
LATIN SMALL LETTER, and it does not appear in any other script).

Prefer using AGL names from the list for new fonts (aglfn).

Glyphs.app names are okay, but some of them are wildly or weirdly
different from their unicode counterparts.
A common example is U+002D HYPHEN-MINUS, which is called
`hyphen` (after AGL); meaning that U+2010 HYPHEN gets called
`hyphentwo`.


## Weight

Please use [CSS Common Weight Name
Mapping](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight#common_weight_name_mapping)
where appropriate.

Reproduced here:

    100 	Thin (Hairline)
    200 	Extra Light (Ultra Light)
    300 	Light
    400 	Normal (Regular)
    500 	Medium
    600 	Semi Bold (Demi Bold)
    700 	Bold
    800 	Extra Bold (Ultra Bold)
    900 	Black (Heavy)
    950 	Extra Black (Ultra Black)

Apart from «950 Extra Black» these are also the weight used in
the Glyphs App export tab of the Info window.

# END
