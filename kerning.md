# Kerning

## Groups

[morpho] means the kerning groups are morphologically
dependently.
Obviously all kerning groups are somehwat morphologically
dependent; this mark is used for some of the more variable
cases.
For example in some fonts, /V and /W share the same slope on
both sides, in some other fonts they do not.

- A + accents, L and R
- /AE + accents, AE | E
- B + accents, H | B
- C + accents, probably O | C
- D + accents above and below, probably H | O probably;
  exceptions for /Eth /Dcroat /DZ /Dz
- /DZ + accents, H | Z
- /Eth /Dcroat, Eth | D
- Dz + accents, H | z
- E + accents, H | E
- /Schwa, no group?
- /Ezh + accents, Ezh | Ezh
- F, H | F
- G + accents, O | G
- H + accents (except /Hbar), H | H
- /Hbar, Hbar
- I + accents, often H | H
- /IJ /Iacute_J.loclNLD, I | J
- J + accents, J | J
- K + accents, H | K
- L + accents (except /Ldot /Lslash), H | L
- Ldot, H + Ldot
- Lslash, no group
- M + accents when sides match, H | H
- N + accents when side match, H | H
- Nhookleft, usually H | H
- /Eng (2 morphs), H | Eng
- O + accents above and below, O | O
- Oslash + accents, Oslash | Oslash [many pairs will = O]
- /OE, O | E
- P + accents, H | P
- Thorn, H | Thorn
- Q [morpho], O | O
- R + accents, H | R
- S + accents, S | S
- Saltillo??
- Germandbls??
- T + accents [including /Tbar?], T | T
- U + accents, U | U
- V + accents, V | V
- W + accents [morpho], V | V
- X + accents, X | X
- Y + accents, Y | Y
- Z + accents, Z | Z [not needed?]

# END
