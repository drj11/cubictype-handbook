# Currency

From https://www.fileformat.info/info/unicode/category/Sc/list.htm

Currency signs based on latin letters. Those marked with @ on
the left are in use (not historical):

- @ DOLLAR
- @ CENT
- @ POUND
- @ CURRENCY, sort of
- @ YEN
- @ U+0E3F THAI CURRENCY SYMBOL BAHT
-   U+20A0 EURO-CURRENCY, historical, for ECU
- @ U+20A1 COLON, Colón, Costa Rica and historical Salvador
-   U+20A2 CRUZEIRO, probably the brazilian ones, historical
-   U+20A3 FRENCH FRANC, historical
-   U+20A4 LIRA, historical, also double-crossbar alt for £
-   U+20A5 MILL, probably historical
- @ U+20A6 NAIRA, Nigeria
-   U+20A7 PESETA, probably historical, rare
- @ U+20A8 RUPEE, ligature, not U+20B9
- @ U+20A9 WON, Korea, for both KPW and KRW
- @ U+20AB DONG, Vietnam
- @ U+20AC EURO
- @ U+20AD KIP, Laos
- @ U+20AE TUGRIK, Mongolia
-   U+20B0 GERMAN PENNY, historical, morph based on German Kurrent d
- @ U+20B1 PESO, Philippines (for the sign)
- @ U+20B2 GUARANI, Paraguay
-   U+20B3 AUSTRAL, historical Argentina
- @ U+20B5 CEDI, Ghana
-   U+20B6 LIVRE TOURNOIS, historical France
-   U+20B7 SPESMILO, historical, weird
- @ U+20B8 TENGE, Kazakhstan
- @ U+20B9 INDIAN RUPEE
- @ U+20BA TURKISH LIRA
-   U+20BF BITCOIN, *sigh*

Of the historical forms, LIRA is commonly available in fonts,
because it is an double-crossbar morph of £.

In addition to the above there are some duplicate forms intended
to fit into the widths of east-asign fonts:

- U+FE69 SMALL DOLLAR
- some FULLWIDTH signs in the U+FFxx block

# END
