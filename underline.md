# Underlines and Strikeout

https://www.harbortype.com/the-state-of-underlines-and-strikethroughs/
seems to be a reasonable summary of the situation. Which is:

- some software ignores the font's setting, or is buggy;
- some software correctly implements underlines and strikeout

Conclusion: it is worth setting them correctly.

To summarise the spec:

- underlinePosition : the top of the underline. Typically negative.
- underlineThickness : thickness (match U+005F)
- strikeoutSize : thickness; match em dash
- strikeoutPosition : position of the top of the strikeout stroke


## Technical

- yStrikeoutSize and yStrikeoutPosition are in the `OS/2` table
  (from version 0)
- underlinePosition and underlineThickness are in the `post`
  table

# END
