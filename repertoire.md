# Glyph Repertoire

CubicType Recommendation for Glyph Repertoire in 2022:

## Recommendation

Letters for Latin script fonts:

- 26 letters, A to Z, plus Ð Þ Æ Œ

Diacritic marks for Latin script fonts:

- above: acute, circumflex, diaeresis, dot above, macron, grave,
  caron, ring, tilde
- below: cedilla

Strong suggestion of also adding ß.

## Discussion

For comparison, Welsh is the only language to be required by law
in the United Kingdom and that uses acute, grave, diaeresis,
circumflex (and dot in i and j only).

# END
