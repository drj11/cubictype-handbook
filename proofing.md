# Proofing with proofers

For more formal feedback create a proofer.

A proofer differs from a specimen,
in that the proofer is designed to facilitate feedback,
and to educe feedback that is specific and actionable.

After [MV2022] a proofer should consist of 3 sections:

- Glyph palette; grid, approx 48 per page
- spacing;
- long form


## Grid palette

Each glyph laid out in a grid, large enough to see clearly and
with enough space around it to give feedback with a pen
(printed out or on digital tablet).


## Spacing

ADVICE FOR LATIN ONLY.

Each glyph spaced between: **HH**, for capitals;
**nn**, for little letters; **00**, for numbers;
**nn**, for punctuation.

Some care over choosing the spacer may be required, for example,
lower case numbers may require spacing between **nn** as well as
**00**.


## Long form

Samples of the text in both large blocks
(to check texture and rhythm) and in typical use.


## REFERENCES

[MV2022] Malou Verlomme, private communication, in ILT Latin
  Type Design 2022.
