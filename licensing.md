# Licensing

Choice of off-the-shelf licences is complicated and may vary
from product to product.
Bespoke licences, taylored from a narrow set, should be
available to those with suitable budget.

## Overview of Licenses in Use

- Click-wrap license provided by distributor, not possible to
  vary. For example when distributing on myfonts.com their
  standard licence terms are used.
- Artistic license. Perhaps an unusual choice for fonts.
  So far this has only been used for Bob Matrix Mono.


## Considerations

It is worth considering the permitted actions granted by
something like the MIT licence:

> to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,

enumerated as:

- to deal
- to use
- to copy
- to merge
- to modify
- to publish
- to distribute
- to sublicense
- to sell

It is not clear to me what "dealing" is.

Typically a commercial font will require a license to use; a
limited license to copy (copying may be restricted to copying
that occurs in the ordinary course of use); a limited license to
distribute. Distribution is usually controlled by the embedding
flags which may permit distribution in a document and may permit
that document to be editable.

End use licences for fonts do not typically permit general
distribution, and typically do not permit merging, modification,
publishing, sublicensing, or selling at all.

### Distribution as value

For fonts distribution is a key right.
Some uses do not require distribution (t-shirts, badges,
magazines, products), but other uses would require distribution
(embedding in an e-book, a video-game, or a web page).
Thus a set of uses is identified for which the right of
distribution may add considerable value or differentation.

Some of the uses above, while not _requiring_ distribution would
be a lot more comfortable if distribution was allowed.
For example it is difficult to imagine making an international
magazine without being permitted to distribute a font from one
organisation to another.
Publishers, printers, 3rd party producers, contract workers will
all require access to the fonts used, and it may be tediously
unworkable for them to acquire exact copies themselves.

### Modification as value

Withholding the right to modify a font allows the licensor to
add value by licensing it separately.

## Permissive

A "permissive" licence could be one that simply offers the same
terms as a commercial licence (deal, use, copy) for no fee.


## Ethical considersations

One of the points of writing this document was to include some
considerastion of the ethics.
Once approach that has gained some traction in the software
community is the [Hippocratic
License](https://firstdonoharm.dev/).
This has gone through various revision, gaining complexity with
each revision.

Version 2.0 looks like a good compromise.
The recent 3.x revisions introduce a lot of specific
terminology and sections, and frankly looks like
the kind of document that you could only use after consulting some lawyers.

Version 2.0 introduces Indemnity and Enforceability clauses.
It also has an auto-upgrade clause;
it states "Licensee has the option of following the terms
and conditions either of the above numbered version of this
License or of any subsequent version published on the
Hippocratic License Website."

My opinion is that for someone who does not control the future
text of the Hippocratic License then this clause adds too much
risk.

# END
