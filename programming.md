# Programming notes

A miscellaneous collections of notes, mostly things that i keep
forgetting or were annoyingly difficult to find out.

Use Go.

## Go

`go.mod` may be in a parent directory, and this is useful, in
`font8`, when there are many `main` programs that use it.

When updating a package, `coff`, and something that uses it,
`ttf8`, it is necessary to change both and compile them
together.
In `go.mod` add/uncomment `replace gitlab.com/font8/coff => ../coff` to
use the on-disk version;
this allows changes to both `coff` and `ttf8` to be developed
and tested simultaneously.

When tested, commit the dependency (`coff`) first and push it.

In the repo that uses `coff` (typically `ttf8`), undo the edit,
and issue
`go get gitlab.com/font8/coff@COMMIT-SHA` to update
the `go.mod` file in the package that uses it.

When building again the package that uses the freshly changed dependency
you will be prompted to `go mod download gitlab.com/font8/coff`
or similar.

# END
