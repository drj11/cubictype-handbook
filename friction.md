# Friction in CubicType

Following from reading http://web.archive.org/web/20220726145210/https://blog.ceejbot.com/posts/reduce-friction/

An evaluation of friction in CubicType

- :webfont:include: preparing CSS for webfonts in HTML
- :specimen:slow: making specimens, of any sort.
- :fonts:slow: The gap from alphabet to font.
- :glyphs:add: Adding glyphs to fonts?
- :tool:create: creating tools is slow.
- :shape:what: Finding out what a typical shape should be.
- :repertoire:what: Deciding what glyph repertoire to support.
- :glyphs:use: Finding glyphs used in context and showings.
- :glyphs:lang: What languages use this glyph?

:specimen:slow the `plak` tool really helps.
Should probably improve this.
For example, better upper and
lower case magic detection;
magically remove non-graphic glyphs (undrawn)

:fonts:slow drawing is one thing, but also lack of clear target.
[Google's glyphsets](https://github.com/googlefonts/glyphsets)
can help with this.
For self-initiated submissions to Google, they require
LatinCore-Plus (Polish, Slovenian, Welsh).
Vietnamese-Plus (that is, double accents, plus some
math/punctuatioin stuff).

:shape:what For the base alphabet Karen Cheng is somewhere
between good to fine.
Keeping the [Design & Shape](design-shape.md) document has been
useful and continues to be useful.
The recently created `panel` tool is useful.


## Research

There are many research ideas which often seem slow to move
forward on.

### Panel

The panel system does help for assessing glyphs/short texts ad
hoc in other fonts.
Observing this, i created an initial `panel` tool and it is
useful, and definitely an area for more work.

The panel tool is web-based;
the command to make an HTML preview:

    panel html Belgium-Zarquon

### Diacritics

An area where shape (:shape:what) and
repertoire (:repertoire:what) come together.
`panel` helps a bit.

`mica` might help a bit.

# END
