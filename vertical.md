# Advice for setting vertical metrics

This advice applies to the vertical metrics for typical Latin
script fonts in Left-to-Right direction.


## Concrete example: Airvent

Released on 2023-08 Airvent represents a reasonable summary of
the current CubicType thinking:

UPM: 1143 (this is unusual and will be rescaled at some point).
Cap height 800 (= 70% of UPM)

- typoAscender 1100
- typoDescender -329
- typoLineGap 0

Range is 1429 = 1.25 × 1143

- hheaAscender 1100
- hheaDescender -329
- hheaLineGap 0

Exactly same as typo

- winAscent 1140
- winDescent 320

The ink yMax and yMin values as reported by `fwin` in the `head`
table, expanded to the next multiple of 20.
As it happens quite close to 1429.

- underlineThickness 110
- underlinePosition -90
- strikeoutPisition 350
- strikeoutSize 110


## Concrete example: Atwin

UPM: 4000, Cap Height: 2800 (= 70% of UPM)

- Ascender 3900
- Descender -1100

Range is 5000 = 1.25 × 4000

Below descender zone = above cap zone = 1100.


## Ancient Advice from Adobe

Adobe have a promotional leaflet from 2006 with the following
definition of _point size_:

"Point size The common way to describe the
size of a font. A font’s point size is the distance
in points from the top of the highest ascender
to the bottom of the lowest descender plus a
tiny gap for legibility."

This is one of the most practical and clear definitions of point
size that i have seen, and
is also practically speaking what a lot of type designs use.

Technically it involves drawing the font so that the highest
ascender to lowest descender (/d to /p, say) fits within the UPM,
leaving a small gap.


## TrueType and OpenType Vertical Metrics

There are 8 values in a font file that control (some aspects of)
vertical spacing.
These are 3 triples of Ascent/Descent/LineGap for each of Hhea,
Typo, Win.
3×3 is 9 of course. The Win triple doesn't include LineGap.
Also of note is that Descent is negative (below the baseline),
except for WinDescent which is non-negative.

The major sources of authority on this matter are:
Google Fonts, Glyphs, SIL, and John Hudson.
Google Fonts and SIL have recommendations directly addressing this;
Glyphs have a tutorial; and John Hudson has a 2022 typedrawers thread.

The Google Fonts advice [GFVERT] provides a reasonable technical
summary: The Win values _must_ exceed (or be equal to) the vertical 
bounds of the font, otherwise graphics are clipped.

Google Fonts also recommends that the Hhea triples equal the
Typo triples so that spacing is consistent between macOS and
Windows.
And that LineGap is 0;
because when it is non-zero Desktop apps
typically add linegap above the line, web browsers typically
split it 50:50 above:below, and DTP apps typically ignore it.
And Ascent should exceed the height of `/Agrave`;
because text layout on macOS will use the height of /Agrave if
it exceeds the UPM.

There is no explicit recommendation in GFVERT for how to size
UPM relative to the drawing.
Implicit is the idea that CapHeight - p-descender is about UPM.

[SILVERT] has similar recommendations and suggests fitting
line spacing to Á.

[HUDSONVERT] has one of the clearest general recommendations on
sizing the overall font to UPM: the vertical span of /d to /p
should be around the UPM (that is, measuring from the height of
/d to the depth of /p): "Typically, they will be just slightly
shorter than the body height"

[HUDSONVERT] sets Ascent and Descent so that they sum to UPM;
splitting the excess from dp-span between them (either 50:50 or
in proportion to the /d height and /p depth).
Then sets LineGap to get a pleasing line spacing.

Some evidence from [HUDSONVERT] that SIL either are or have
recently changed from Win == Typo to Win defining ink bounds and
Typo to set the satisfactory spacing. (SIL updated in 2022-10)

Most sources in the last few years seems to regard Microsoft's
[MSVERT] advice as outdated.
In particular this advice seems outdated now:
"It is often appropriate to set the sTypoAscender
and sTypoDescender values such that the distance (sTypoAscender
- sTypoDescender) is equal to one em. This is not a requirement,
however, and may not be suitable in some situations".

The same advice from Microsoft also suggests quite strongly that
the distance from the top of frame to first baseline is
sTypoAscender, and the distance from last baseline to bottom of
frame is (negative) sTypoDescender. Which strongly implies that
the LineGap space only appears _between_ rows of text.

In fact ambiguity over where the LineGap space goes seems to be
a good reason to set it to 0 (as many recommendations suggest).
By adding it sTypoAscender and sTypoDescender as appropriate.


## Some notes from [TD2805]

Some people use different metrics for Desktop and Web fonts.
In particular: on Web TypoLineGap is 0; on Desktop TypeLineGap
is > 0.
with TypoAscender - TypoDescender + TypoLineGap being the same.

Ray Larabie: highest point (/Aring) to lowest point (/Aogonek) is
1200 or slightly less.
Stacked Vietnamese accents go higher.

UI fonts may require a much wider script support _and_
stricter adherence to line-spacing metrics.

Thomas Phinney: recommends including space for stack Vietnamese
diacritics, even if they are not initially designed.

Stacked diacritics that are mentioned: /Acircumflex + grave;
/Aringacute; /Ecircumflex or /Abreve with hookabove or tildetone.

Thomas Phinney: in older fonts, cap-height is 70% (of UPM);
ClearType era (Calibri) about 63%.

Helvetica as it ships on my Mac, clips /Acircumflex + acute at
the top of a TextEdit document.


## REFERENCES

[ADOBETT] https://www.adobe.com/studio/print/pdf/typographic_terms.pdf

[GFVERT] Google Fonts recommendations for setting metrics;
  https://github.com/googlefonts/gf-docs/tree/main/VerticalMetrics

[GLYPHSVERT] https://glyphsapp.com/learn/vertical-metrics

[SILVERT] https://silnrsi.github.io/FDBP/en-US/Line_Metrics.html

[HUDSONVERT] https://typedrawers.com/discussion/4554/on-vertical-metrics-in-the-context-of-a-biscript-tamil-and-latin-font

[MSVERT] https://learn.microsoft.com/en-us/typography/opentype/spec/recom

[TD2805] https://typedrawers.com/discussion/2805/font-metrics-settings-for-desktop-and-web-fonts

# END
