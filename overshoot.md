# Overshoots

Type design manuals are remarkably coy on giving specific
quantiable measures on the question of how much overshoot to
use.

## VAG Rounded Regular

- UPM 1000
- Cap Height 712 (height of /E)

All over- and under-shoots are 6 FDU. Present on /O /A /V /H).
Note that rounded terminals of vertical strokes, like /H, also
get overshoot.

6 FDU is 6‰ of UPM and 8‰ of Cap Height.


## Eurostile Regular

(an old TTF off the Microsoft Works CD)

- UPM 2048
- Cap Height 1365 (height of /E) = 666‰

On /O over- and under-shoot is 14 FDU.
Not present on /A /V (flattened apexes),
nor /H (horizontal terminated vertical strokes).

14 FDU is 7‰ of UPM and 10‰ of Cap Height.


## Red Hat Display Regular

- UPM 1000
- Cap Height 700 (height of /E)

On /O over- and under-shoot is 11 FDU.
Not present on /A /V (flattened apexes),
nor /H (horizontal terminated vertical strokes).

11 FDU is 11‰ of UPM and 16‰ of Cap Height.


## Overpass Regular

- UPM 2000
- Cap Height 1400 (height of /E), exactly 700‰

On /O over- and under-shoot is 24 FDU.
Not present on /A /V (flattened apexes),
nor /H (horizontal terminated vertical strokes).

24 FDU is 12‰ of UPM and 17‰ of Cap Height.


## Fira Sans

- UPM 1000
- Cap Height 689 (height of /E)

On /O over- and under-shoot is 12 FDU.
Not present on /A /V (flattened apexes),
nor /H (horizontal terminated vertical strokes).

12 FDU is 12‰ of UPM and 17‰ of Cap Height.


## Courier Prime Regular

- UPM 2048 (traditional TTF)
- Cap Height 1187 = 580‰ of UPM (seems quite low)

On /V over- and under-shoot is 22 FDU. Present on /V.
Not present on /A (flat-topped with left-pointing flag), nor /H
(serifs with flats, no cups).

22 FDU is 11‰ of UPM and 19‰ of Cap Height.


## BBC Reith Sans Regular

- UPM 1000
- Cap Height 715 (height of /E)

On /O over- and under-shoot is 15 FDU. Not present on /A
/V (flattened apexes), nor /H (horizontal terminated vertical
strokes).

15 is 15‰ of UPM and 21‰ of Cap Height.


## Heuristica Regular

- UPM 1000
- Cap Height 692 (height of /E)

On /O over- and under-shoot is 15 FDU.
Not present on /A /V (flattened apexes),
nor /H (serifs with flats).

15 FDU is 15‰ of UPM and 22‰ of Cap Height.


## Avenir Next

- UPM 1000
- Cap Height 708 (height of /E)

On /O over- and under-shoot is 18 FDU.
Not present on /A /V (flattened apexes),
nor /H (horizontal terminated vertical strokes).

18 FDU is 18‰ of UPM and 25‰ of Cap Height

# END
