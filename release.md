# Release procedure for font

Mostly see ranalina/build.md


## branches and versions

All releases are made on a short stub branch take from `main`.

Depending on the toolchain used for the font, the version number
of a font is:

- YYYYMMDD—this form usually appears in the font's name
- YYYY.MMDD—this form appears in the fonts version field, _if
  the toolchain supports it_ (most current conventional tools do
  not; `ttf8` does but this is only used to build _Ranalina_
  currently)
- Y.JJJ—where Y is the current Y minus 2020 (2 for 2022) and J
  is the day of the year (from 001 to 366, see `date +%j`)

In the case that you are making a release several days after the
most recent change to the font, you can use the date of the most
recent change to the font.


## build output

Primary build output is a font-file, typically `.otf` or `.ttf`.
There may also be license files, welcome notes, specimens,
user guides, and so on.

Some assets may be prepare automatically by Continuous
Integration infrastructure.
For example, _Avimode_ has a specimen file created
automatically.


## procedure

    git checkout -b version/0.YYYYMMDD

- Change name to remove `LAB`
- check version in name is correct
- check version in version field is correct
- check Designer field is filled in and correct
- check Manufacturer field is filled in with _CubicType_
- check Manufacturer URL field is filled in with _https://about.cubictype.com/_
- check Copyright (on Glyphs, how?)
- placeholder license text is
"No distribution permitted, except according to your proper licence agreement."
- check Description field is filled in. If short, can be
  something like "A _adjective_ font from CubicType. Made in
  Yorkshire by David Jones."
- Check Sample Text is filled in. Multiple lines work, but are
  awkward to edit in Glyphs. Best prepare text in TextEdit and
  paste it in.
- Check Vendor ID is filled in (with "CUBI" for a CubicType font).

Export font (in Glyphs.app) to OTF file.
Ensure Remove Overlap and Autohint are enabled.

Check all this by running

    otfinfo --info newfont.otf

The old and new OTF file names depend on the version.
Remove the old one and add the new one from the git repository.

    git rm oldfile.otf
    git add newfile.otf


Check you are on the fresh release branch and commit that.


## Release artefact

A release artefact will typically be a zip file containing:

- licence (see licence/)
- read-this.html (see doc/)
- font files
- specimen

Create a fresh sibling directory named after the font and
version. For example `Airvent20230801`.
In this copy the above items, editing the licence.

Make a zip file.

Back in `git` select the main branch and add the zip to the
release directory.

Distribute.


## Itch.io platform

Itch.io have their own image asset requirements:

https://hedgiespresso.itch.io/itch-page-image-templates

- cover image 630×500: thumbnail for social links, and so on.
  Technically 315×250 up to 630×500, but why would you do
  something that small? Or, can it be bigger, that's just the
  recommended size?
- screenshots 3840×2160 maximum. Probably. I think it only tells
  you when you try and upload one that exceeds that. This is
  16:9 "4K"; double 1920×1080
- banner (i haven't used one yet). 900×400 px? It _replaces_
  your title.


## Monotype Platform

Monotype Platform requires a description which is a typically a
few paragraphs of text.
Can use bold and italic and hyperlinks.
It cannot be longer than 500 characters. *sigh*

Monotype Platform also requires 5 to 15 images
(displayed in a carousel) that are:

- 2:1 ratio
- 2000×1000 pixels (or at least 1440×720)
- PNG format


### Updating a family

Updating a family can be done by pressing the Edit Submission
button when viewing a submission.

You will need new version of all the regular files (OTF and PNG)
and a change note (previous purchasers can get the new version).


# END
