# Font Engineering

Font Engineering covers aspects of the font design and creation
process that are governed by software and tools, as opposed to
the artistic/graphic side.

Included withing Font Engineering is Font Production and Release.
Which is the business of exactly what font files to make and how
to deliver them as products.

Engineering should comply with existing good practice and
standards.


## Vendor Name

As of 2023 CubicType has a registered Vendor ID of "CUBI".
This can be set in Glyphs with the "vendorID" Custom Parameter
(in Font Info).


## Name table

When a new font is installed using Font Book (macOS) it previews
a sample of the font in a dialogue with the "Install" button.
The preview text can be chosen by the font: Fill in Name ID 19
of the OpenType `name` table.

In Glyphs this can be set with a "Name Table Entry" Custom
Parameter (in Font Info).

For example:

    19;Thank you for choosing CubicType
    ABCDEFGHIJKLMNOPQRSTUVWXYZ
    0123456789

It is possible to put NEWLINE characters in this, but
it is a bit awkward in Glyphs as the input box is only one line.
Either pasting in, or using Alt-Enter works.

# END
