# Specimen

This is under development.

A Specimen is a document intended to reveal, explain, and
showcase a font.
It often appears alongside a font (in its `.zip`) or available
at the point of retail.

The audience and intended purpose are important.

A specimen may be intended:

- for a retail font, to display its positive aspects and
  entice people to buy and/or use the font;
- for a custom font, to display its positive aspects and
  explain to a client what they have bought and why it is so
  expensive;
- for an in-development font, to highlight any features that may
  be of interest to type-designers, type-buyers, and also to
  make prominent any feature or aspect that the author wishes to
  discuss;
- for any font, to provide a subject for discussion in a
  presentation/workshop/salon.

Therefore a font may have several specimens for different
purposes at any moment or throughout its lifecycle.


## PDF

Specimen files are PDF files.
They may be different for print and screen.

Prefer landscape orientation for screen.


## Content

Cover page that includes author, date, and
the full font name including version (a standard CubicType font
will have the version in its name).

A colophon (back page) detailing the fonts used.

For latin fonts, a display of the alphabet and the numbers.

For completeness, at least one showing of every glyph.

Any interesting useful OpenType Layout features should be
highlighted.

A waterfall is useful.
Small and medium sizes can be presented
on a single page.
Large sizes can use a single page.
Adjust line-spacing to save space.

For body text i have found the introduction to
the UN Declaration of Human Rights to be useful because
it is availabe in many languages.

For Atwin the languages used are:
English, Icelandic, Maltese, Romanian, Slovenian, Welsh.

For neutral body text (where using the target font is not
sensible) use Catamaran.

# END
