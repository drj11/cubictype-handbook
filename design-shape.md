# Some notes on the design of shapes.

A not very well organised collection of my notes on how to draw
particular glyphs in a font.
Mostly analphabetics,
because Karen Cheng covers the letters and numbers.

Mostly in Unicode codepoint order and with official Unicode in
all caps.
Other names will be taken from AGLFN or Glyphs or
other in-use conventions.

[KLCS] is essential reading. As well as having good opinions on
repertoire, it also has good opinions on feature rules for
localisation and design models for many glyphs.

_Brill_ is a reference to the font family Brill which
has a large repertoire.


## .notdef

Please draw a `.notdef` glyph.
It is required to be called `.notdef` and be numerically Glyph
Index 0.
Standard tools will do this automatically if the glyph is called
`.notdef`.

The Cubic Type design standard for this is an outline Glyph with
3 contours:
- a square black box from baseline to cap-line;
- two reversed-out strokes that overlap to make an X inside the box;
- orientation of the strokes will necessarily mean that they
  interfere in the middle creating a black square in the
  intersection, according to the non-zero winding rule.

I encourage you to draw the box and stroke in the style of the font.

Rationale: This is "rectangle with X" variation
[suggested by
Microsoft](https://docs.microsoft.com/en-us/typography/opentype/spec/recom#glyph-0-the-notdef-glyph)
but drawn in a way that it would be difficult to confuse with
existing Unicode characters.

The standard Microsoft `.notdef` is a monoline X drawn inside a
box so that the X touches the corners.
It is confusable with U+1F771 ALCHEMICAL SYMBOL FOR MONTH.


## U+0020 SPACE

Please design this and don’t leave it as the Glyphs default.


## U+0021 EXCLAMATION MARK, exclam

A dot with a vertical stroke above it.
The dot is (almost always) the same as the period
(in size and position).

In designs with contrast, the vertical stroke is tapered
(narrow at the dot end) and reaches a width similar to
the vertical stroke width in H (usually a bit more).
The taper can be varied: teardrop with an ogee (Linotype Bodoni Roman),
straight (Linotype Didot Roman), convex (Cooper BT Bold).

In designs without contrast, the vertical stroke may be straight
or tapered, sometimes with a cornered straight/vertical design
that looks like a centre-punch.

Terminals are usually serifless and straight or rounded.
Occasionally matching the terminal style of the body font.

The exclamation mark aligns to the cap-height (with optical
overshoots).

The inverted form is \exclamdown U+00A1 and uses the same design
rotated;
see separate entry.


## U+0023 NUMBER SIGN, hash, hashtag, /numbersign

Same height as /H.
Always drawn obliquely, with the crossbars indented to the same
oblique angle (almost always).
Apart from the basic design and stroke width being consistent with
the body font, there is rarely any other design connection.


## U+0024 DOLLAR SIGN, /dollar

The model for this is a full-height /S with a vertical stroke through it.
There is a lot of variation.

According to https://web.archive.org/web/20230620202428/https://learn.microsoft.com/en-us/typography/opentype/otspec181/euro
"Traditionally numerals and currency symbols are
the same width for any given font" (implied is that this is for
tabular numbers).

- $ can be an exact copy of the /S (Sabon)
- similar to /S, but less high (Tisa, where it is matched to smallcaps)
- similar to /S, but less wide (DIN)
- less commonly the $ has different terminals (Filosofia Regular,
  Century Schoolbook)

The vertical stroke is often not continuous and commonly appears
as a short vertical projection from the top and bottom.
Occasionally a more graphic in-out _snake_ effect is used (Stencil,
ITC Benguiat).

The vertical extent of $ is sometimes aligned to cap-height, but
commonly projects, extending above the cap-line and descending
below the baseline, but not as far as a descending lower case
letter.

The angle of the vertical stroke matches the font style (that is,
sloped when the font is italic or oblique).
The thickness of the vertical strokes is
often much thinner that regular vertical strokes,
making it the thinnest stroke.

Some showings in MDA have a small dollar sign (and cent sign)
either solely or as an alternate to the regular size dollar.
For example, Vigus (Davison Arabesque), Urban, Sintex.
They may be simple photo-reductions.

In MDA the smaller dollar signs hang from a top-line,
essentially appearing as superscript if small enough.

For small dollar sign, consider U+FE69 SMALL DOLLAR SIGN or
making a smallcaps `.smcp` version.


## U+0025 PERCENT

If fractions have been drawn then `frac` 0/0 can be used,
at least as a starting point.
Most designs will be able to use a shorter 0 that is more nearly
circular, and this will allow a more compact %, by
bringing the 0 horizontally towards the fraction slash.

Normally drawn to the height of caps / lining numbers.

You may as well draw U+2030 PER MILLE SIGN while you're here.
It can be drawn as a percent
with an additional small 0: ‰.

And i just discovered the, even wider, U+2031 PER TEN THOUSAND.

Sometimes there are connecting flourishes / arcs.


## U+0026 AMPERSAND How to draw ampersand

(From reflection-2021-w20.md)

My current advice for drawing the usual 2 counter form is:    
    
- Stack two circles vertically,
  with the bottom one touching the baseline and
  the top one toching the cap-line.
  The circles may overlap, touch, or be separate, according to style, but
  usually the top one is smaller than the bottom one.
  The left-hand edge of the top circle will usually be to the right of the
  left-hand edge of the bottom circle.
- Draw a diagonal stroke that cuts the bottom circle and is tangent to the
  top circle.
  The counter of the bottom circle is usually substantially bigger than a
  half the circle.
- Remove unused parts of the circles, add a crossing stroke to make the
  upper teardrop, and add tails.
- Adjust.


## U+0027 APOSTROPHE, quotesingle

Should be symmetric and designed to work with both upper and
lower case (the typographic apostrophe is
U+2019 RIGHT SINGLE QUOTATION MARK).

The top is usually the cap-line (sometimes a small amount above,
sometimes a small amount below);
the bottom is usually slightly below the ex-line
(sometimes not, which looks terrible).

Taper and shape usually shared with exclamation mark.


## U+002A ASTERISK

Asterisk has a number of punctuation and symbol roles.
In running text it used as a footnote or highlight marker, even
when not in superscript position.

In computer codes it used as a unary and a binary operator, and
in markdown text it is used in pairs.

There are a variety of basic designs and positions commonly seen.
For positions:

- in raised position, hanging from approximately the same
  top-line as the double quote or the capital top-line.
- in median position, consistent with /plus /minus /divide /multiply.

The raised position is attractive because then the regular
Asterisk can be re-used for U+2051 TWO ASTERISKS ALIGNED VERTICALLY,
and the low asterisk from that can be used for U+204E LOW ASTERISK,
effectively giving two heights of asterisk.
Those heights with offset horizontal positions can then be used
for U+2042 ASTERISM which is conventionally 3 asterisks in a
1-above-2 arrangement.

The basic design is a star, usually with either 5 or 6 points.
The little blue book [GLYPH] claims "The original shape of an
asterisk was seven armed".

In most cases the repeated point motif is tapered, being
narrower at the centre of the star;
this creates a hub-and-spoke effect.
If the taper is reversed, the effect is to make a more
traditional star shape.

6-spoke designs have 2 points aligned either vertically or
horizontally.
Weirdly i think there may be a slight tendency to see 6-spoke
designs with horizontal bars on keyboards and vertical bars
elsewhere.

5-spoke designs are like a typical human doing star jumps.
One point is aligned pointing upwards.
An exception is Titillium Web which is symmetric (as typical)
with a point pointing East.

- Zilla Slab, 6-point raised vertical alignment
- Nunito, 6-point raised horizontal alignment
- Fira, 5-point raised
- Fira Code, 5-point median
- Ubuntu, 5-point raised
- Plex Mono, 5-point median
- Adobe Source Code Pro, 5-point median
- Overpass, 5-point raised
- Overpass Mono, 6-point median horizontal alignment
- JetBrains Mono, 5-point median
- Alegreya, 5-point raised, notable for having asymmetric spokes
  that do not touch.

Size varies a lot:

- Raleway around 25% or less of cap-height
- Fira Sans, Overpass around 50% of cap-height
- IBM Plex Mono slightly > 50% of cap-height

Occasionally the centre is cleared.
In IBM Plex Serif
the spoke motifs don't quite meet in the middle.

Occasionally star designs with other numbers of points are used.
8 being the most common.
For example Space Mono, and Pellucida M 8.

As well as the LOW, TWO, and ASTERISM forms already mentioned,
Unicode has many other asterisk and star symbols.


## U+002F SOLIDUS, /slash

Please kern (even though lots of old/system fonts don't).

Sometimes drawn with a thin stroke (even in fonts with no
significant thick/thin contrast: Lucida Sans Unicode).

Sometimes descends (slightly), and slightly less often also
ascends.


## U+003A COLON

A starting basis for the colon is to use two replicas of the
period dot.
The top one should align with the top of the x-height
(with optical overshoot in most cases).

Variations exist and it's not unusual to see one.

For example, in regular to light fonts with large x-height, the
dots might seem too far apart, so lower the top dot.

Consider an OpenType feature `case` for capitals, and
something for between numbers (where the colon would raised to
make it more central vertically).


## U+003B SEMICOLON

Semicolon almost always exactly the same upper dot as COLON, but
over a comma.


## U+003C LESS-THAN SIGN and U+003E GREATER-THAN SIGN

Symmetric angles.
Several uses:

- in maths for a comparison operator;
- in HTML for an angle bracket (in text or maths it would be
  better to use U+2329 LEFT-POINTING ANGLE BRACKET or
  U+27E8 MATHEMATICAL LEFT ANGLE BRACKET);
- for separating deep menu items `File > Export > Choose Format`
  and occasionally other hierarchical items;
- general decoration / pauses / spaces. >>>>>

Generally vertically centred on the same line as + = - and other
mathematical operators.
Generally not quite as tall as lining numbers
(a stroke or two shorter).

Typically acute angle.

Some designs have < and > touching the baseline,
though this is not the most common.

Typically looks terrible with lower case numbers (Zilla Slab).

Important to distinguish from single and double
ANGLE QUOTATION MARK.
Typically the quotation mark is shorter and aligns with ex-line.


## U+003F QUESTION MARK

A cap-height hook with a stem, positioned above a dot.

The height is the same of capitals.

The dot is almost always exactly the same as U+002E FULL STOP
(sometimes cognate, but slightly smaller).

The hook has a horizontal extremum at the right.

The vertical position of this
extreme point is usually slight above the ex-line.

Most designs have at least a short section of stem that is
vertical immediately above the dot.

Of the designs that have a vertical stem above the dot,
some have a vertical stem and a distinctly angled knee, others
have a smooth ogee between the vertical stem and the vertical
tangent at the right-hand extreme.
A few designs have a very short vertical stem or
a zero-length stem (Times New Roman),
but still maintaing a vertical tangent.

Some designs (Optima, Century Gothic) have an S-shaped hook
(a reversed S),
with a greatly diminished lower part.
In those designs the tangent at the lower stroke ending
is merely somewhere to the right or upwards.

The position of the top-left terminal of the hook varies a lot,
as does the terminating angle any decoration.
It is generally consistent in style with the rest of the font.

Thick/thin contrast generally follows the hand of the font.
If the font has an axis that is perpendicular to the spine of
the hook, or more perpendicular than vertical, then the spine
will be thin, otherwise, thick.


## U+005E CIRCUMFLEX ACCENT, asciicircum

Top aligns with number height (often same as capitals).
Always bigger (taller, wider, more obtuse angle) than
a circumflex (as a modifier letter or a combining mark).
It's height apparently bears no relationship with the latin
letter metrics (above or below ex-line, above or below H
crossbar, and so on).

Microsoft treat this as a maths operator (as in ASCII 2^5==32),
and so [they recommend using the same advance width as figure
space](https://learn.microsoft.com/en-us/typography/develop/character-design-standards/math),
along with all the other maths operators.


## U+005F LOW LINE, underscore

You almost never hear the term LOW LINE.

The ancient Microsoft Character Design Standards recommend

- vertically placing this 5% to 10% of the UPM below the baseline;
- width of en-space;
- extending full width so that it connects continuously when repeated.

Should it be cognate with the header metrics? Who knows.

In Fira Sans, which does not have EN SPACE (or EN QUAD), underscore is the
same width as EN DASH.

Worth noting:

- many fonts don’t do this.
- enough fonts get the underscore metrics wrong that
  Firefox browser has per-case overrides for them
  (hunt around in the about:config stuff).


## U+0069 LATIN SMALL LETTER I

Should be drawn as a composite of `/idotless` U+0131 and `/dotaccent`,
as per https://glyphsapp.com/learn/diacritics.

See also U+0131 LATIN SMALL LETTER DOTLESS I for Turkish support.


## U+007C VERTICAL LINE, bar

In thickness is usually between the thin and thick strokes (in
fonts with thick/thin contrast).
Vertical range varies, but
usually from around the bottom of /p to the top of /d;
which also makes it usually around 1-em in height.


## U+007E TILDE, asciitilde

From left to right, goes up down up, ending up more or less at
the starting vertical position.

Usually more-or-less level on average, without an obvious
_swing_.

Usually centered at the middle of Cap-height.
Often distinctly bigger (wider) than the diacritic tilde.

It may be reasonable to draw it as vertically shifted copy of
U+223C TILDE OPERATOR aka /similar.

Anchors for mark positions above and below will allow
U+2E1E TILDE WITH DOT ABOVE and U+2E1F TILDE WITH DOT BELOW to
be drawn.
This may be problematic though; U+2E1A U+2E1B U+2E1E U+2E1F are
dictionary punctuation to be used with stem forms and so, and
therefore act like a sort of hyphen.

See also U+2053 SWUNG DASH


## U+0085 NEXT LINE (NEL)

(`NEXT LINE (NEL)` is the old Unicode 1.0 name of this
codepoint)

Probably not required, and if present should be blank.

`fontbakery` has a check for this.


## U+00A1 INVERTED EXCLAMATION MARK, exclamdown


The inverted exclamation mark (U+00A1) is the same design as
\exclam but rotated.

[Glyphs argues very strongly for having two vertical
positions](https://glyphsapp.com/learn/localize-your-font-spanish-inverted-question-and-exclamation-marks):
one for "mixed case" and one for "capitals".
Using the `.case` tag.

`\exclamdown.case` (for capitals) will range as capitals.
`\exclamdown` should have the top of its dot at the x-line.
(with overshoot);
possibly raised slightly more if needed to fit.


## U+00A3 POUND SIGN, sterling

Pound sterling currency sign (used in the UK and other
countries).

A fancy **L** with a horizontal crossbar.

A double crossbar is also acceptable, but note:

- Bank of England uses a single crossbar and has done for decades;
- Most conventional digital-era fonts use a single crossbar;
- Double crossbar version should be at U+20A4 LIRA SIGN if drawn.


## U+00A4 CURRENCY SIGN, currency

A circle drawn in a style consistent with the font, with
4 projections at NE, NW, SW, SE.

Microsoft suggest it is centred between 0 (/zero), and this is common.
It is occasionally seen on the baseline or in superscript
position.


## U+00A6 BROKEN BAR, brokenbar

Usually the same design as U+007C VERTICAL LINE but with a
substantial break (around 2 stroke widths) in the middle,
making the top and bottom parts equal.

Seems wrong when it isn't the same length as VERTICAL LINE (in
Overpass for example).


## U+00A7 SECTION SIGN, section

A ligature of two **s** forms.
It is not a copy of either **s**, but
usually has a companion form.
Terminals are more often than not shared with the **s**
(but not in the case of Times New Roman, Bodoni, Souvenir).
It is a descender, but need not descend as much as the Latin
lowercase descenders (Helvetica, Optima, Bodoni, Souvenir).
The counter is most often either an almond shape with asymmetric
positions points (often a very oblate almond), or an
ellipse/racetrack/circle with no points.

Exercise caution when using samples to determine position and
extension of section.
Across the Helvetica brand the section varies:
Helvetica Now has it descending, but notably not as far as /g /y;
Helvetica Neue it descends the same as /g /y;
Helvetica World has it descending almost, but not quite, the same amount
as /g /y.
Samples taken from fonts.com.


## U+00AB U+00BB DOUBLE ANGLE QUOTATION MARK, guillemet

My favourite quotation marks
(the only balanced quotation marks in the U+00xx block of unicode).

Consider two vertical positions, one for lowercase, one for
capitals; use `case` OpenType Feature.
This seems to be fairly rare however (Tekton, Ubuntu).

Vertically aligned with the middle of the ex-height.
Vertical span varies between around 50% of ex-height
(Helvetica, URW Franklin Gothic) to 100% of ex-height (Times New Roman,
Eurostile), with most somewhere in between.
Design varies between foundry issues of same/similar fonts.

Angle varies, with tall designs being more obtuse than less-tall
designs.

Nose is usually pointy, but not always (Helvetica is not).

Tapering is usually consistent with rest of font.

Asymmetric contrast is only common on poetic fonts (Carmina, Mirarae).
Pretending that the strokes are hands of a clock,
the usual latin calligraphic stress applies:

- For «: thin for the 1 o'clock hand, thick for the 5 o'clock hand
- For »: thin for the 7 o'clock hand, thick for the 11 o'clock hand

In formal fonts the double angle quotation marks are
always two copies of the same flechette.

More varied in display and informal fonts.


## U+00AC NOT SIGN, logicalnot

Vertical position is conventionally centered on the math
operator median used for + − ×.

However, consider the more radical option of placing it in
superscript position, where a mathematician would write it
(consistent with other unary operators such a unary negation
and unary complement (tilde)).


## U+00AD SOFT HYPHEN

Unicode 14 [TUS14] §6.2:

«Despite its name, U+00AD SOFT HYPHEN is not a hyphen, but
rather an invisible format character used to indicate optional intraword
breaks.»

Therefore, do not draw.
Some legacy systems may require a glyph that is present but has
no graphic, but for 21st century systems the recommendation is
that the glyph should not be present.

More discussion on typedrawers https://web.archive.org/web/20230809083802/https://typedrawers.com/discussion/comment/26655/

`fontbakery` has a check for this.


## U+00B6 PILCROW SIGN, /paragraph

Modelled on a **P** reflected to face left.
With a doubled vertical stroke that usually descends somewhat,
but not always to the descender line.
Usually the "bowl" is filled and counterless but not always.

Sometimes (Helvetica) the two vertical strokes are tied at the
top with a short crossbar.

See also U+204B REVERSED PILCROW SIGN (which now faces the same
way as **P**).


## U+00B7 MIDDLE DOT, periodcentered

Usually the same dot as FULL STOP, but raised to the "middle" of the
cap height.
The Adobe Glyph Name for these two glyphs is `period` and aperiodcentred`,
which makes this relationship clear.

Should use same vertical centering as U+2022 BULLET,
but appears not to in some fonts.

Requires OpenType Features in Catalan to integrate with
L WITH MIDDLE DOT.


## U+00BF INVERTED QUESTION MARK, questiondown

A rotated /question.
See INVERTED EXCLAMATION MARK for vertical alignment notes.


## æ ae U+00C6 LATIN CAPITAL LETTER AE U+00E6

A letter in icelandic and other European languages.
A discretionary orthographic ligature in English
(Archæologist, and so on).

The capital letter Æ is an /A fused with an /E.
Almost always the /A is slanted to the right to join the /E.
This makes the left stroke of the /A more slanted and
makes the right stroke vertical and shared with the /E.

Futura is a well-known example where the /A is unchanged and the
/E slants to the left to join it.
The full title to Jacobi Bernoulli’s “GRAVITATE ÆTHERIS” also
has a leftward leaning /E fused to a familiar /A.

Compared to the form of the standalone **A**,
the amount of space occupied by the **A** part of **Æ** is
approximately the same.
Sometimes the A’s crossbar is raised to be unified with the /E
(Souvenir, Oranda), but mostly not.

Sometimes the form of the **A** is changed slightly in the **Æ**:

- short horizontal top-stroke making a trapezoidal counter
  (Helvetica, Eurostile);
- leftward pointing serif at top (Times New Roman, Bodoni);
- modified terminal or stroke (Souvenir, Maiandra)

Older, 20th Century, fonts may not kern **Æ** properly,
which is unacceptable in the 21st Century.


## U+00D0 LATIN CAPITAL LETTER ETH, eth

An icelandic letter.

The upper case form is a /D with a short
horizontal stroke, and is generally exactly the same as the
Croatian D with Stroke (U+0110).

The stroke weight should be the same or similar to the horizontal
stroke of the **H**.
The stroke should extend on the right to the middle of the
counter, and be shorter on the left.
In a serif font the stroke will typically extend leftwards to
the same point as the serif.

The lower case form generally uses the uncial form of the lower
case d (curly d), even when the rest of the font is roman.
The uncial d should have no foot and the “vertical” stem should
curl over the bowl.
In highly geometric or stylised fonts, the curve in the stem may
not be possible.
Consider creating some morphological distance
from the roman d by removing the foot.

Note on Icelandic: if you draw `/eth`, also draw U+00DE `/thorn` and `/ae`.

Some notes from an Icelandic type designer
(unfortunately, captured without images):
https://web.archive.org/web/20170628030256/http://briem.net/2/2.11/index.htm


## U+0131 LATIN SMALL LETTER DOTLESS I

See https://glyphsapp.com/learn/localize-your-font-turkish for
Turkish support.

If smallcaps are drawn, `/idotless.smcp` _and_ `/idotaccent.sc`
are required (for Turkish and other languages).


## U+0138 LATIN SMALL LETTER KRA

aka non-AGLFN `/kgreenlandic` in Glyphs.app.

The [Wikipedia article on Kra](https://en.wikipedia.org/wiki/Kra_(letter))
has a good illustration of how graphically near KRA is to smallcaps `/k` and
Cyrillic ka; and also how typical fonts make the distinction
poor at best.

I suggest starting with a truncated `/k`;
since the smallcaps `/k.sc` will be based on the Capital `/K` form,
a truncated `/k` is already likely to be a little bit different.

Note: no separate uppercase form.


## U+013B LATIN CAPITAL LETTER L WITH CEDILLA, /Lcommaaccent

and some other letters with cedilla/comma.

Note that the unicode name is "WITH CEDILLA", but the
conventional form for this glyph is to draw the accent as a
comma.
Hence the Glyphs name of /Lcommaaccent.

This is for Latvian orthography.

In
[Marshallese](https://en.wikipedia.org/wiki/Cedilla#Marshallese)
orthography, a true cedilla is preferred.
The Wikipedia article is surprisingly helpful.

If you draw `/Lcommaaccent.loclMAH` in Glyphs (draw it with a
cedilla), it will add the appropriate feature code to `locl`.

Glyphs also suggests `/Ncommaaccent.loclMAH`, and
for little letter **l** and **n** too.

The Wikipedia article suggests a couple more letters, so maybe
more are needed.


## U+013F LATIN CAPITAL LETTER L WITH MIDDLE DOT

and 0140 LATIN SMALL LETTER L WITH MIDDLE DOT
aka `/Ldot` `/ldot`; _punt volat_.

`koeberlin` recommends
`/L_periodcentered.loclCAT` `/l_periodcentered.loclCAT` as do i.

Should be designed, and to be identical to their composed form
L + U+00B7.

Spacing, and so on.

Glyphs article:
https://glyphsapp.com/learn/localize-your-font-catalan-punt-volat


## U+0141 LATIN CAPITAL LETTER L WITH STROKE

and U+0142 LATIN SMALL LETTER L WITH STROKE

Slight, not steep angled thin stroke.
More like 30° and not as steep as 45°.

positioned slightly to the right on **l**, more
to the right on **L**.

Unique to Polish. Adam Twardoch has some good drawing advice:
http://www.twardoch.com/download/polishhowto/stroke.html
(for the lowercase; capital letter not yet discussed).


## U+014A LATIN CAPITAL LETTER ENG

Default form should be cap-height **n** with the small letter shape.
On the recommendation of `koeberlin`
https://github.com/koeberlin/Latin-Character-Sets#eng-%C5%8B

Add `/Eng.loclNSM` for Sami localisation.

According to Microsoft's (ancient) design standards
https://learn.microsoft.com/en-us/typography/develop/character-design-standards/uppercase
the cap-height **n** ("round top") is used by _Hausa_, but
[Hausa's Wikipedia
page](https://en.wikipedia.org/wiki/Hausa_language) does not
list it in the Latin-derived alphabet.


## U+017F LATIN SMALL LETTER LONG S, /longs

Typographically, an **f** with the right-hand part of the
crossbar removed.
It is often exactly this with no other adjustments made.

There is no separate capital form, the capital form is **S**.

Historically long s also forms the model for the left-hand part of
U+00DF LATIN SMALL LETTER SHARP S, which is an orthographic
ligature of long s and **s**.

If using long s in the drawing for sharp s then typically the
right-foot of the serif is removed, and the top arch is extended.

It has one accent, a dot above:
U+1E9B LATIN SMALL LETTER LONG S WITH DOT ABOVE.
_Brill_ puts the accent mark above the vertical stroke.

There are a couple of other non-combined variants (WITH DIAGONAL
STROKE and WITH HIGH STROKE), and a presentation form ligature
(U+FF05).

## U+01B7 LATIN CAPITAL LETTER EZH and U+0292 LATIN SMALL LETTER EZH

`/Ezh` `/ezh` (non-AGLFN)

A "Z with curly tail".
Not well enough represented to easily find a good range of models.

It superficially resembles 3 (`/three`) Ȝ (`/Yogh`) З (Cyrillic `/Ze`).

In Brill the uppercase Ʒ has the same height as **Z** and shares
the upper half with **Z** (possibly slightly condensed).
The lower bowl terminates on the left with a bulb terminal at a
similar height above the baseline that the upper serif is below
the cap line.

In Times New Roman the Ʒ descends and has a pointed termination.

The left-extremities are the top-stroke and the bottom-bowl and
are both similar, with the top-stroke slightly to the right;
a form consistent with **Z**.
The internal apex is to the right, considerably so in Brill
(putting the apex approximately halfway left-right), slightly so
in Times New Roman.

The little letter has similar shape,
it can be thought of as **z** + **g**:
top-half modelled as top-half of **z**, bottom half is a bowl that descends,
approximately following **g** (but more open).
The internal apex is significantly above the baseline.

CARON / haček can be added on top.


## U+01E8 LATIN CAPITAL LETTER K WITH CARON and U+01E9 LATIN SMALL LETTER K WITH CARON

Used in [Skolt
Sami](https://evertype.com/alphabets/skolt-sami.pdf) and
according to Wikipedia, a transliteration of Laz.

For Capital K the caron mark should be centered, but for little
**k** this is less clear.

A brief sample of existing fonts that support it:

- American Typewriter, centered
- Arial Unicode, centered
- Avenir Next, on the stem
- Chalkboard SE, centered
- Charter, centered
- Geneva, centered
- Lucia Sans Unicode, centered
- Menlo, centered
- Ubuntu, on the stem but nudged to the right

In the Everson PDF (above), the ǩ is drawn twice.
In the title the mark appears centered;
in the alphabet the mark appears at the x-height.

At the x-height is clearly the radical option, although,
like ĥ, that is the position you would get on a traditional
typewriter.

For now, CubicType recommends at the x-height if possible.
Using a narrow mark shared with /i /j is recommended.


## U+0192 LATIN SMALL LETTER F WITH HOOK

This is a letter in the Ewe language, so should be drawn
consistently with the other letters, in particular **f**.

It has been co-opted for Dutch Florin (and other national
Florins in the Caribbean), so
there are a lot of fonts that draw this in italic style.
Drawing it italic in an upright font means that the font can’t
be used for Ewe.
This probably originated in Adobe calling this /florin.

Sometimes also used for f-stop in camera apertures.

See https://github.com/adobe-fonts/source-sans/issues/42

Paraphrasing John Hudson from
http://web.archive.org/web/20230919120151/https://typedrawers.com/discussion/3642/florin-vs-latin-small-letter-f-with-hook-u-0192
: draw it as a letter; italic fonts supporting Ewe will need
both a hookless /f and this hooked f.


## U+0272 LATIN SMALL LETTER N WITH LEFT HOOK

Vertically, descends to same extent as `/g`.
Seems to use the same hook as `/eng` (U+014B), but
on the left-hand leg of `/n`.


## U+0302 COMBINING CIRCUMFLEX ACCENT

Required for Welsh ("pŵer" meaning "power" for example).

This accent appears on some Latin ascenders.
For example ĥ, h with circumflex, used in Esperanto.

In this particular case [the Wikipedia page for Esperanto
Orthography](https://en.wikipedia.org/wiki/Esperanto_orthography#Origin)
suggests an origin in typewriter dead-keys which would put the
circumflex over the arch of the **h**.


## U+030C COMBINING CARON aka Haček aka Caron

The accent has a different form on **d** **l** **L** **t**.
See http://diacritics.typo.cz/index.php?id=5 for some details.

In Gylphs use `caroncomb.alt`, see
https://glyphsapp.com/learn/localize-your-font-czech-and-slovak-vertical-caron
.

For Ľ the top of haček is commonly the cap-height, but some
designers (Peter Biľak) prefer a slightly raised haček.
In a font where the little letter ascenders exceed the cap-height, then
the ascender-height may be suitable.

ILT article also useful
https://web.archive.org/web/20221130000000*/https://ilovetypography.com/2009/01/24/on-diacritics/

### Anchor placement for `/caroncomb.alt`

Malou Verlomme (priv. comm.) recommends that `/d` should have
its `topright` anchor exactly on the right hand side of the stem.
Then the corresponding anchor on `/caroncomb.alt` will be
to the left of the drawing, and the gap sets the space
between the stem and the mark.
Adjust the anchor on `/caroncomb.alt` to simultaneously adjust the space
for all glyphs that use it.


## U+0326 COMBINING COMMA

See http://diacritics.typo.cz/index.php?id=9 and the
Romanian/Turkish cedilla/comma saga (i think Glyphs handles this
basically automatically).

Generally slightly dimished from the text comma.
Detached from the base letter, but possibly very close.


## U+0328 COMBINING OGONEK, ogonekcomb

Critical points:

- does not extend to the right of the base letter;
- descends about as far as a regular descender.

Adam Twardoch has some old but
[good notes on Polish ogonek and other accents used in Polish](http://www.twardoch.com/download/polishhowto/index.html)


## U+1E03 LATIN SMALL LETTER B WITH DOT ABOVE

Glyphs places the automatic anchor in _compact position_: below
the top line, above the bowl.
Other designs (Brill, Fira Sans) place the dot above the
ascender top line, horizontally centered over the entire glyph.
Not centred over the stem.

Liberation Mono uses compact position.

It is not a well represented character.
It's used in the historical form of lenition in Irish.


## U+1E10 LATIN CAPITAL LETTER D WITH CEDILLA, /Dcedilla

also U+1E11 LATIN SMALL LETTER D WITH CEDILLA.

Glyphs composes this with a comma accent.
A very rough survey also suggests comma is more common.

Brill draws it with a comma accent.

Note: unlike L WITH CEDILLA, Glyphs does not change the name.


## U+1E20 LATIN CAPITAL LETTER G WITH MACRON, U+1E21 SMALL LETTER

Required by Uzbek orthography, apparently. http://web.archive.org/web/20210728000750/https://twitter.com/cyrillicsly/status/1420174094306217988

Filled in by `mica` principle on most Latin fonts.


## U+1E24 LATIN CAPITAL LETTER H WITH DOT BELOW, /Hdotbelow

also U+1E25 LATIN SMALL LETTER H WITH DOT BELOW

Seems to have the dot placed centrally, unlike H WITH CEDILLA.
See U+1E28 LATIN CAPITAL LETTER H WITH CEDILLA for notes on
anchor placement.


## U+1E28 LATIN CAPITAL LETTER H WITH CEDILLA

also 1E29 LATIN SMALL LETTER H WITH CEDILLA

The cedilla accent is positioned below the stem of the left leg.
Other accents below are positioned below the middle of the bowl.

This means two different below anchors are required.
Since H also has BREVE and LINE BELOW,
the special-case anchor should be cedilla.

The Glyphs.app tutorial explains how to do it:
https://glyphsapp.com/learn/advanced-diacritics-multiple-anchors

- On /H and /h create a `bottom` anchor in the middle, for use by
  breve and macron, and create a `bottom_cedilla` anchor at the
  bottom of the left stem.
- If needed, add a `_bottom_cedilla` anchor to the (top of)
  /cedillacomb.
- In /Hcedilla and /hcedilla use the anchor selector (info panel
  of glyph in edit mode) to select the `bottom_cedilla` anchor.


## U+1E9E LATIN CAPITAL LETTER SHARP S

German orthography now accepts this as a capital version of ß
where previously SS would be used.
Official adoption varies by country.

Judging by the Wikipedia page i can say the following:

- wider
- increase in stroke thickness/value consistent with other capitals
- large open round bowl in lower part, due to extra width
- long arch on top leading on the right too:
- a marked angle join between the arch and descending stroke.
- remove left crossbar on left-hand stem (the /longs)

Nice example on book cover:

https://images.booklooker.de/x/00pjVr/Horst-Klien-Hrsg+Der-gro%C3%9Fe-Duden-W%C3%B6rterbuch-und-Leitfaden-der-deutschen-Rechtschreibung.jpg


## U+2007 FIGURE SPACE

It is as wide as a number.
Glyphs makes it as wide as **0** by default.
Which seems fine.

The Unicode Spec [TUS14] is surprisingly helpful, §6.2:

> U+2007 figure space has a fixed width, known as tabular width, which is the same width as digits used in tables. U+2008 punctuation space is a space defined to be the same width as a period. U+2009 thin space and U+200A hair space are successively smaller-width spaces used for narrow word gaps and for justification of type.

The same section is also helpful for other dashes and spaces.


## U+2012 FIGURE DASH

Same as U+002D HYPHEN-MINUS but as wide as (tabular) number.
See also U+2007 FIGURE SPACE.


## U+2013 EN DASH; U+2014 EM DASH; U+2015 HORIZONTAL BAR;

Increasingly wide dashes.
Should be at the same height and width.

Some designs may wish to consider the Microsoft design
recommendation that they have square cut ends and 0 side
bearings, so that they join seemlessly.

Although nominally 1-em wide, which would be the vertical
distance between ascender and descender in a typical font,
the EM DASH is rarely that wide.

The EN DASH should be half the width of the EM DASH and often is.

The HORIZONTAL BAR (aka QUOTATION DASH) is used to set off
quotes in some languages preferred typography.
Opinions vary as to the extent that this actually happens, and
the glyphs ideal width.

https://web.archive.org/web/20230809083802/https://typedrawers.com/discussion/comment/26655/

The existence of U+2E3A TWO-EM DASH and U+2E3B THREE-EM DASH
suggests that U+2015 HORIZONTAL BAR should not be as long as 2
EM DASH at least in fonts that have U+2E3A TWO-EM DASH as well.

I found a typedrawers thread which suggested that HORIZONTAL BAR
is sometimes 1.5 EM DASH, but also Hudson makes the point that
it could be literally a /bar that is horizontal (making it
slightly less than 1 em).
What typographers call /bar Unicode calls U+007C VERTICAL LINE,
which removes support for the Hudson design.


## U+2020 DAGGER

The traditional dagger is drawn in the shape of a latin cross,
with a short handle, and two projecting guards drawn
horizontally (at right angles to the blade) and
approximately the same length as the handle.
The blade is much longer.

Conventionally it is placed vertically, with the handle at the
top.

Overall length is approximately the same as þ, and
it is sometimes drawn in the same position: as a "lower case"
glyph that descends and ascends.
But not always, and other positions are seen.

Brill vertically positions the dagger from cap-line to descender
line and puts the crossbar level with the x-line, which
i think is deliberate.


## U+2021 DOUBLE DAGGER

Two copies of a dagger "handle" joined by a blade (that has no point).
Conventionally the top handle is a copy of the handle
from U+2020 DAGGER and is in exactly the same place.


## U+2022 BULLET

Usually a circular dot substantially bigger than period.
Commonly bigger than two H-thick widths.
Centred between base-line and cap-line (usually);
means that the top of the bullet has no particular relationship
with the x-line—it can be at (Times New Roman), above
(Palatino), or below (Grand Arcade).

Spacing varies from almost none to about as much space
(divided between lsb and rsb) as the width of the dot.

In P22 Operina (aka Trattatello) the bullet is a slightly skew
left-pointing triangle centred between base- and x-line.
So decorative and reactionary possibilities exist.


## U+2026 HORIZONTAL ELLIPSIS

Three dots.
I see no reason why it shouldn’t have exactly the form of three
FULL STOP glyphs set adjacent to each other.

Although at least one website
https://practicaltypography.com/ellipses.html suggests that the
ellipsis should be in width between «...» and «. . .» (3 FULL
STOP and 3 FULL STOP with word spaces in between).

[Microsoft
suggest](https://learn.microsoft.com/en-us/typography/develop/character-design-standards/punctuation)
"The advance width should be set on the em space." If they
actually mean one em width then that will generally be too wide.

Space evenly so that successive runs have the same space between
dots (following Microsoft's suggestion).

See also U+2024 ONE DOT LEADER and U+2025 TWO DOT LEADER.


## U+2033 RAISED DOT, U+2034 RAISED COMMA

RAISED DOT is described in Unicode 15.0 as
«glyph position intermediate between 002E . and 00B7 ·»;
RAISED COMMA not explicitly described, but i presume it to
correspond to RAISED DOT.


## U+204A TIRONIAN SIGN ET

The basic form is a more or less horizontal stroke, with
a vertical stroke on its right that descends to the left.

Several variations exist. [Apparently Tschichold has a whole
page of
them](https://stancarey.wordpress.com/2014/09/18/the-tironian-et-in-galway-ireland/).

In modern use (which seems to be Pay & Display machines in Ireland),
it used for “and” in Irish, following the use of «&» in
other languages.

This is the lower case version.

Good examples are hard to come by.

Mostly, the horizontal stroke aligns with the ex-line, or
slightly below.
The vertical stroke descends to either the descender line, or,
more ugly, to the baseline.
Neither stroke is required to be exactly horizontal or vertical.
It seems that the horizontal stroke is either horizontal or very
nearly so, and may have a slightly arc (descending) between two
ends that are horizontal.
The vertical stroke varies between exactly vertical, nearly
vertical (descending to the left), or distinctly sloped
(descending to the left).

Unicode has U+25E2 TIRONIAN SIGN CAPITAL ET which is presumably
sized and vertically aligned to fit with capitals, but
i've never seen one.
Some discussion on typo.social in 2023 suggests that the capital
ET is extremely dubious and has no support in documents.


## U+2053 SWUNG DASH

Consider making this to fit with U+2010 HYPHEN (true hyphen),
and used as the basis for the dictionary tilde + mark forms
U+2E1A U+2E1B U+2E1E U+2E1F.


## U+20B9 INDIAN RUPEE SIGN

See also U+20A8 RUPEE SIGN (generic) and various other
government rupee sign.

The Indian government approved Rupee sign (the rupee as a word
and a currency is used by various cultures and governments).

Graphically it is the right-hand part of an **R**
(the bow of the bowl and the leg) with an additional double
horizontal stroke at the top and the bowl median.
Often the horizontal strokes are slightly lighter than the
typical stroke width.


## U+20A4 LIRA SIGN, lira

Wikipedia describes it as uncommon, and
provided for compatibility with HP Roman-8.
But it is in WGL4.

A version of £ with a double stroke.


## U+220F N-ARY PRODUCT, product

Microsoft recommend vertically aligning to `summation`, so
from cap-height at top to approx ½ descender at bottom.

Drawn as a slightly enlarged Greek capital pi, possibly stylised.


## U+2211 N-ARY SUMMATION, summation

Microsoft recommend vertically aligning to
cap-height at top to approx ½ descender at bottom.

Drawn as a slightly enlarged Greek capital sigma, possibly stylised.


## U+221A SQUARE ROOT, radical

The point of the tick should be on the baseline (with
overshoot).
The left hand arm should be short, and may have a lead in
stroke.
The right-hand arm should reach over the numbers.
Presumably it could/should meet OVERLINE if there was one.


## U+223C TILDE OPERATOR /similar

This _should_ be the tilde when used as a mathematical operator
(usually used to indicate a binary relation that may be an
equivalence relation).

In the font used on Wikipedia, U+223C is drawn bigger (wider and
taller) and with more swing, and is positioned very slightly
higher.

The same shape appears as a component in a number of other
mathematical operators (group isomorphism and so on), so
it should be drawn in combination and to ally with _less than_,
_curly less than_, _equals_, and so on.

It does not seem to be commonly supplied in fonts.

See also U+223D (the same but flipped), and U+2241 (the same
with a slash through to indicate _not_).


## U+2248 ALMOST EQUAL TO

Cognate with **=** U+003D EQUALS SIGN.

Consider using U+007E TILDE as a component.


## U+25CC DOTTED CIRCLE

This is commonly used as a placeholder when showing combining marks.
For example in the UI for Glyphs and Wikipedia articles like
https://en.wikipedia.org/wiki/Diacritic and the official Unicode
charts like this http://www.unicode.org/charts/PDF/U0300.pdf

The geometry is an exact circle made of either short arc strokes
or dots. There does not seem to be any particular number of
components.

Because it is used to display combining marks, it should
have at least all the anchors used by the combining marks in
the font.


## U+263F MERCURY

See Planets.


## U+2E13 DOTTED OBELOS

The [Wikipedia obelos
article](https://en.wikipedia.org/wiki/Obelus) suggests many
forms of obelos.

The most common of these already have fairly stable and specific
Unicode characters:

- U+00F7 DIVISION SIGN (horizontal bar, dot above, dot below)
- U+2020 DAGGER
- U+2052 COMMERCIAL MINUS SIGN (a SW–NE diagonal bar with a dot
  either side)

There are approximately two macOS installed fonts that support
U+2E13: Helvetica (and Helvetica Neue) and Geneva.

Geneva draws U+2E13 like COMMERCIAL MINUS SIGN, but not quite
the same design.

Helvetica draws U+2E13 as a short horizontal bar with a dot
above. Which would be similar to U+2A2A MINUS SIGN WITH DOT
BELOW except that Helvetica doesn't supply that glyph.

Since COMMERCIAL MINUS SIGN exists and has a stable design, it
seems to mean that one possible use of U+2E13 DOTTED OBELOS is
for the alternate design: a short horizontal stroke with a dot
above.


## U+A78B LATIN CAPITAL LETTER SALTILLO; U+A78C LATIN SMALL LETTER SALTILLO

Wikipedia: https://en.wikipedia.org/wiki/Saltillo_(linguistics)

Unicode Latin Extended-D https://www.unicode.org/charts/PDF/UA720.pdf

Neither source is particularly useful in drawing the glyphs.

I have drawn them
(in Arhinni, using the /caroncomb.alt as a distant model):

- the width of a vertical stem (approximately)
- symmetric
- tapered: a long slight taper at the top for most of its
  length, and a rapid stronger taper at the bottom, ending in a
  blunt cut.
- in the capital, vertically extending from cap line (with a
  small overshoot), to midline (centre of **F** crossbar
  approximately)
- in the small letter, vertically extending from the top of the
  **t** (approximately), to the top of the inside of the arch of
  **n**; making it approximately the height of two thick
  horizontal strokes and with its mid point on the x-line.

### Andada

The (now SIL) font _Andada_ is worth examining: https://andada.huertatipografica.com/

Andada uses the same drawing for lowercase and uppercase saltillo;
the uppercase saltillo is raised.

The general form is a more or less vertical stroke consistent with
vertical strokes in the other letters.
The stroke is at a slight angle consistent with the angled serifs in /a and /r.
The stroke has a taper, narrower at the bottom, but
with more rapid tapering at the top.
Not a convex shape (Arhinni’s is convex), somewhat like a very narrow funnel.

The overall height is approximately 80% of the x-height;
it is considerably more than the ASCII apostrophe, and
the U+2019 RIGHT SINGLE QUOTATION MARK.

### Words

Used by languages like Tlapanec and Rapa Nui.
Some suggestions for adopting in other ortographies, for example
Emberá.

These are words in Emberá:

kꞌosima
tꞌosima
pꞌata


## FEMALE SIGN, MALE SIGN, Planets and so on

Mercury and other planets have a set of Unicodes in the
[Miscellaneous Symbols
block](https://en.wikipedia.org/wiki/Miscellaneous_Symbols).
Planets start with
MERCURY at U+263F through to EARTH at U+2641, finishing
with NEPTUNE at U+2646 and the trans-Neptunian object Pluto at
U+2647.

Font support is quite variable.
Browsing Wikipedia on macOS,
the body text is set in Helvetica;
it does not have any of the planetary symbols.
The symbols are rendered in:

- Mercury STIX General
- Venus Arial
- Earth STIX General
- Mars Arial
- Jupiter STIX General
- Saturn STIX General
- Uranus (U+2645) Apple Symbols
- Pluto STIX General

Note on coding: The Unicode sequences and names are not always
particularly logical or useful.
U+263F MERCURY is the first planet and is followed in sequence
by the remaining planets (in system sequence) and
then U+2647 PLUTO.
However: Uranus, Neptune, Pluto are each coded twice because they
each have two different symbols in (reasonably) common use:
one is a graphic, the other a monogram.

U+2645 URANUS codes the monogram, based on **H** for Herschel.
The graphic symbol, which fits more with Eart, Mars, on so on,
is U+26E2 ASTRONOMICAL SYMBOL FOR URANUS.

U+2646 NEPTUNE codes the two symbols essentially the other way
around: U+2646 is the graphic symbol (a trident);
the **LV** monogram (for Le Verrier) is U+2BC9 NEPTUNE FORM TWO.

Pluto (not a planet, but a Trans-Neptunian Object) has the
monogram form at U+2647 PLUTO and the graphic form (a bident) is
U+2BD3 PLUTO FORM TWO (there are _three_ more; Unicode lists a
total of 5 form for Pluto, see your Unicode shop for details).

The Unicode names for Venus and Mars are in fact U+2640 FEMALE SIGN
and U+2642 MALE SIGN.

Regarding the above font selection,
presumably Arial is preferred as a substitute for Helvetica,
but Arial only has Venus and Mars
(also the Linnean symbols for female and male).
STIX General (a sans serif with wide coverage) gets used for the rest,
except URANUS U+2645 which is mysterious missing from
STIX General.

STIX General does however have the other (graphic) form of Uranus
U+26E2 ASTRONOMICAL SYMBOL FOR URANUS.

Various other solar system bodies are encoded in this block from
U+26B3 CERES to U+26B7 CHIRON and the made-up body
U+26B8 BLACK MOON LILITH.
None of those render in my macOS setup in 2022.

STIX General is the only font so far that seems to support
the basic planets (and even then, U+2645 is missing).
And STIX General is typographically speaking a bit of a mess
when it comes to planets.

VENUS EARTH MARS ♀♁♂ all have a circle, and thankfully they
share the same size and stroke.
VENUS and EARTH are exact flips of each other.
It seems like most of the symbols are vertically centred on a
horizontal line that does not particularly relate to the latin
baseline, x-height, cap-line, or top-line.

EARTH, a circle with cross joined on the top,
does not sit on the baseline, it descends but not to the baseline.
The top of the cross nearly reaches the cap-height.
VENUS (cross at bottom) uses exactly the same vertical zone, so
the cross descends an awkward amount, the crossbar does not sit
on the baseline, the circle does not quite reach the cap-height.

MERCURY, which is Venus with an arc on top, both ascends and
descends more than Venus, meaning that its circle does not align
with Venus.
MARS, which has an arrow pointing NorthEast, is very slightly
smaller in total height than Earth, and consequently its circle
is very slightly shifted down.

JUPITER SATURN NEPTUNE are all vertically similar in size, all
ascend above the cap-height and descend below the baseline, but
have features that align with the baseline and the cap-height.
I suspect this is coincidence, but
it suggests that it would be reaonable to use that as a design guide.

On that basis PLUTO, a P L ligature, looks a bit big.

ASTRONOMICAL SYMBOL FOR URANUS looks like it should be MARS
(circle + arrow) pointing vertically up and with a dot.
But it has a lighter circle, a heavier arrow, and uses a
different vertical zone.


## Other astronomical objects and signs

Mercury, Venus, Earth, Mars, Uranus suggest an orb/cross +
additional symbol system.

Scanning https://en.wikipedia.org/wiki/Astronomical_symbols
for other bodies that could fit into this system (many on that
page do not have Unicode codepoints, and some have other forms
that i don't mention):

Asteroids:

- U+26B3 CERES a crescent blade above a cross
- U+26B4 PALLAS a diamond above a cross (spear)
- U+26B5 JUNO an 8-way star above a cross
- U+26B6 VESTA in one form, horns above a box
- U+2BDA HYGIEA (10) a caduceus: an open circular arc on a circle
  on a circle overprinted on a vertical rod

and maybe

- U+2BD9 ASTRAEA (5) two orbs joined by virgule, a percent sign
- U+2698 FLOWER could be used for Flora (8), a circle above a
  petalled stem, but probably no requirement in Unicode to use
  this particular form.

Trans Neptunian Objects:

These are a mixture of monograms and/or based on circular
symmetric/asymmetric forms or other divergent forms not fitting
a stacking system Venus/Earth.

Zodiac and constellations

You might want to consider the 12 Zodiac symbols. They have
Emoji presentations on social media systems.

Some of the Alchemical Symbols are based on the Astronomical symbols


## Alchemical Symbols

These are in general extensive and diverse in form.

Unicode Working Group 2 document N3584 provides a lot of useful
background and extracts of scanned primary sources and
manuscripts: https://www.unicode.org/wg2/docs/n3584.pdf

Some are based on Astronomical symbols: the planets correspond
to metals and their ores and salts use the same symbols with an
additional symbol (like marks and accents in Latin script).

Some use a similar circle/cross stacked system:

- Cobalt, circle with half-cricle below
- Manganese, circle with **T** above and struck through
- Sulfur, triangle above cross.
- and so on

Some are monograms / ligatures:

- /Q_E  U+1F700 ALCHEMICAL SYMBOL FOR QUINTESSENCE
- /Aqua_F  U+1F705 ALCHEMICAL SYMBOL FOR AQUAFORTIS
- /Aqua_R U+1F706 ALCHEMICAL SYMBOL FOR AQUA REGIA
- /A_R U+1F707 ALCHEMICAL SYMBOL FOR AQUA REGIA-2
- /V_S U+1F708 ALCHEMICAL SYMBOL FOR AQUA VITAE
- /ezh_ezh U+1F713 ALCHEMICAL SYMBOL FOR CINNABAR (?)
- /T_R U+1F748 ALCHEMICAL SYMBOL FOR TINCTURE
- /C_X U+1F74C ALCHEMICAL SYMBOL FOR CALX (or just /C ?)
- /E U+1F757 ALCHEMICAL SYMBOL FOR ASHES (?)
- /a_a_a U+1F75B ALCHEMICAL SYMBOL FOR AMALGAM
- /S_S_S U+1F75C ALCHEMICAL SYMBOL FOR STRATUM SUPER STRATUM
- /s_s_s U+1F75D;ALCHEMICAL SYMBOL FOR STRATUM SUPER STRATUM-2
  (or use /longs_longs_longs, or both ?)
- /f U+1F761 ALCHEMICAL SYMBOL FOR DISSOLVE (not sure about the
  base symbol for this one; on Wikipedia it looks like an
  integral around a half circle)
- /V_A U+1F767 ALCHEMICAL SYMBOL FOR CRUCIBLE-3
- /T 1F768;ALCHEMICAL SYMBOL FOR CRUCIBLE-4
- /A_V U+1F76A ALCHEMICAL SYMBOL FOR ALEMBIC
- /M_B U+1F76B ALCHEMICAL SYMBOL FOR BATH OF MARY
- /M_V U+1F76C ALCHEMICAL SYMBOL FOR BATH OF VAPOURS
- /ezh_s_s U+1F772 ALCHEMICAL SYMBOL FOR HALF DRAM
- /ounce_s_s U+1F773 ALCHEMICAL SYMBOL FOR HALF OUNCE

I'm not sure if i can get away with single letters:

- /E ASHES
- /f DISSOLVE
- /T CRUCIBLE-4


## Brackets, braces, and parentheses

No one seems to tell you how high these should be or where they
should be centred.

Options range from exactly the baseline to the capline to
substantially overshooting both of those lines.
Mainstream fonts mostly seems to encapsulate the
“Latin Glyph Zone”, so from the descender-line to a bit above
the cap-line.
Lining up with cap-lines or descender-lines happens in some
fonts, but not in others.


## Currency signs

There are quite a lot of these.

Of my fonts, Airvent has the widest support with:

    baht cedi cent currency dollar euro hryvnia
    lira naira rupeeIndian sheqel sterling yen

Microsoft standards recommend that when a font has tabular
figures (numbers) that the currency symbols also use the same
advance width if the design supports it.

Many of the currency symbols are based on a letter struck
through with one or two lines.
Often the choice of one or two lines is stylistic, not
set by any particular rules or standards.

See POUND / LIRA for a case where Unicode conventionally encodes
the one- and two-stroke forms at separate codepoints.

### Unencoded currency signs

Those who wish a more adventurous selection of currency symbols
may want to note the following currency symbols that i’ve found
that do not have Unicode codepoints (yet):

- U double slashed: Used for Baum in the Alex Kidd videogames
  https://alex-kidd.fandom.com/wiki/Baum
- P double slashed across the lower stem: Used for Pokémon Dollar
  https://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_Dollar
  (note /ruble is similar, but single slash; Philippine Peso is
  a P with a double slash through the bowl).
- S with the middle part of the spine replaced with a circle:
  Used for The Simoleon from The Sims videogame series.

- Livatu (one fully curved boar’s tusk) and the related Tuvatu
  (paper currency) both have currency signs based on
  the shape of U+2316 POSITION INDICATOR:
  a circle centred on a larger plus.
  Livatu has vertical ticks projecting upwards from the ends of
  the West and East arms;
  Tuvatu has projecting ticks that go left from the North arm and
  right from the South arm (the banknote pictured on the
  Wikipedia page has the mirror image of this).
  https://en.wikipedia.org/wiki/Livatu


## Fractions

Common practice seems to be to define ¼ ½ ¾ as standalone glyphs.
The OpenType `frac` feature, where present,
should map `1/4` (and so on) to these via a GSUB rule.

The `numr` and `dnom` GSUB features map regular numbers to
numerator (above fraction slash) and denominator (below fraction
slash) forms.

Common for there to be just one "small version" of each number,
and to only vary the Y position to make `numr`, `dnom`, inferior, and
superior forms.
This can be done with GPOS rules or separate glyphs
(possibly component glyphs).

Vertical space is at a premium.
Common for the vertical range of the `numr` and `dnom` forms to be
exactly half the range of capital E or H, or a tiny bit more
(Red Hat Display Regular, 350/700 = 50.0%).
Also common for the vertical range of the `numr` and `dnom`
forms to be substantially more than half (PT Sans Caption,
436/700 = 62.3%).

Tal Leming, OpenType Feature expert, has a
[blog post called Fraction Fever
2](http://web.archive.org/web/20130325033504/http://talleming.com/2009/10/01/fraction-fever-2/)
with their favourite fraction feature rules.

Some useful stuff on fractions (and more on the other sorts of
numbers) here: https://web.archive.org/web/20220311160200/https://www.fontshop.com/content/figuring-out-numerals

Glyphs.app has the following article: https://glyphsapp.com/learn/fractions

Glyphs.app automatically adds some `frac` feature code when
`.numr` and `.dnom` glyphs are present.
That `frac` feature code:

- converts slash to fraction
- converts all numbers to their `.numr` form
- converts any `.numr` that follows a fraction or follows a
  `.dnom` to `.dnom` form

This is fairly simple, but means the designer has to select the
text to style (possibly using a regex).
Tal's scheme is much more automatic.

## Inferior and Superior numbers

The, ancient, [Microsoft Typography Character Design
Standards](https://web.archive.org/web/20230430204803/https://learn.microsoft.com/en-us/typography/develop/character-design-standards/figures)
suggest:

Based on full-sized numbers, scale 0.63 horizontally and 0.60
vertically (and adjust for grey value).

For inferiors, the _text_ says drop inferior numbers below the
baseline by approximately half the inferior number's height.
But then gives an example where the drop is about 30%.

A lot of the Microsoft document is difficult to interpret.

Both inferior and superior numbers have Unicode codepoints:
- superior 1, 2, 3 have codepoints in Latin-1 Supplement, and
  the remaining superior numbers are in U+207x.
- inferior numbers are in U+208x.

An informal survey of fonts on my Mac:

- Times: only has superior 1, 2, 3. aligned at top with /E.
- American Typewriter: tops are about ½ stroke above /E.
- Andale Mono: only has superior 1, 2, 3. aligned at top with /E.
- Apple Chancery: top of /E is approximately same as a superior
  numbers midline that is slightly above halfway, approximately
  coincided with middle strokes of 3 5 8.
- Arial Unicode MS: aligned at top with /E.
- Avenir Next: only has superior 1, 2, 3. aligned at top with /E.
- Catamaran: only has superior 1, 2, 3, 4(!). Tops are about 2
  stroke widths above /E (about 20% of number height).
- Comic Sans: only has superior 1, 2, 3. Advance width is too
  large. Tops are about a stroke width above /E.
- Helvetica Neue: By mistake? 1, 2, 3, align with top of /E; 4
  through 9 rise above by about 25%. lol.
- Hoefler Text: rise above by about 25%.
- Infini: Aligned at top with /E.
- Lucida Grande: Rise above by about 30%.
- Tekton: Rise above by about 25%. Selection highlight suggests
  vertical metric problem?
- Tratatello: Aligned at top with /E.
- Ubuntu: Aligned at top with /E.


## REFERENCES

[MDA] Modern Display Alphabets, Paul E. Kennedy.

[TUS14] The Unicode Specification, Version 14.

[KLCS] Koeberlin Latin Character Sets,
  https://github.com/koeberlin/Latin-Character-Sets

[GLYPH] Glyph: A Visual Exploration of Punctuation Marks and
  Other Typographic Symbols; ISBN  9781908714282


# END
